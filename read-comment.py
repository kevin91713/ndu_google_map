import csv
import jieba

# 開啟 CSV 檔案
with open('GoogleMapComment.csv', newline='',encoding='utf-8-sig') as csvfile:

  # 讀取 CSV 檔案內容
  rows = csv.reader(csvfile)
  #設定字典
  jieba.set_dictionary('dict.txt.big.txt')
  # 以迴圈輸出每一列
  for row in rows:
      print('===========================================================================')
      print("評論者姓名: " + row[0])
      seg_list = jieba.cut(row[2], cut_all=True)
      print("全模式: " + "/ ".join(seg_list))  # 全模式

      seg_list = jieba.cut(row[2], cut_all=False)
      print("精準模式: " + "/ ".join(seg_list))  # 精準模式

      seg_list = jieba.cut(row[2])  # 精確模式
      print("精確模式: " + " ".join(seg_list))

      seg_list = jieba.cut_for_search(row[2])  # 搜索引擎模式
      print("搜索引擎模式: " + " ".join(seg_list))
      print('===========================================================================')
      print("\r\n")