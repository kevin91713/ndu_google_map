import random
import time
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import Select
import csv
from selenium import webdriver
from selenium.webdriver.edge.service import Service
import pyautogui as pag


# 必須過濾的資料
def replaceChar(text):
    text = text.split('\n')
    # 刪除個人評論總筆數
    text.pop(1)
    if len(text) > 3:
        # 刪除最後兩個不必要的元素
        text.pop()
        text.pop()
        mergeText = text[2:]
        text = text[0:2]
        mergeText = ''.join(str(i) for i in mergeText).replace('=', '').replace('，', ' ').replace(',', ' ').replace(' ',
                                                                                                                    '').replace(
            ']', '').replace('\'', '').replace('[\'', '')
        text.append(mergeText)
    else:
        text.append("無")
    return text


# 你下載的WebDriver.exe檔案路徑
path = Service("D:\\Users\\kevin91713\\Downloads\\edgedriver_win64\msedgedriver.exe")

# proxy =
# opt = webdriver.ChromeOptions()
# opt.add_argument('--proxy-server=' + proxy)
driver = webdriver.Edge(service=path)
# 進入網址
driver.get(
    "https://www.google.com.tw/maps/place/%E7%8E%89%E5%B1%B1%E5%9C%8B%E5%AE%B6%E5%85%AC%E5%9C%92/@23.4443943,121.0313213,17z/data=!4m7!3m6!1s0x346edf7afc18cf61:0x900cc892465fcc1b!8m2!3d23.4443894!4d121.03351!9m1!1b1?hl=zh-TW")

# 啟動延遲(先讓網頁所有資料載入完成才可以開始取得資料)
time.sleep(5)
# 畫面所有資料顯示
# print(driver.page_source)

# 取得總頻論數量
totalNum = driver.find_elements(By.CLASS_NAME, "jJc9Ad")
print(len(totalNum))
# print(totalNum)
# totalNum = totalNum[:]
for i in totalNum:
    print(i.text)

