import nltk
from nltk import FreqDist

text1 = 'John likes to watch movies'
text2 = 'John also likes to watch football games'
all_text = text1 + " " + text2

words = nltk.word_tokenize(all_text)

freq_dist = FreqDist(words)
print('\r\n'.join(str(i) + ' ' + str(j) for i, j in freq_dist.items()))

