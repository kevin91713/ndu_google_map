# NDU Google Map Project

## 專案說明
### 本專案是專門爬取Google評論上的內容，其餘使用的軟體請自行處理。

## 開發環境與套件說明
1. Python 3.10 (最高只能到3.10.X，超過3.10會有套件異常)
2. WebDriver(請根據自己的瀏覽器版本去做設定與下載) [WebDriver教學](https://medium.com/marketingdatascience/selenium%E6%95%99%E5%AD%B8-%E4%B8%80-%E5%A6%82%E4%BD%95%E4%BD%BF%E7%94%A8webdriver-send-keys-988816ce9bed)
3. requests : 請求資料用
4. fake_useragent : 更換標頭套件
5. pyautogui : 透過Python控制滑鼠
6. selenium : 瀏覽器自動化

## 更新Python內部套件
```bash
python.exe -m pip install --upgrade pip
```
## 專案安裝步驟
1. 安裝 requests
```bash
pip install requests
```
2. 安裝 fake_useragent
```bash
pip install fake-useragent
```
3. 安裝 selenium
```bash
pip install selenium
```
4. 安裝 pyautogui
```bash
pip install pyautogui
```

## 專案需搭配使用軟體
### VPN(本專案使用NordVPN)

## Issue 追蹤平台
### 待補

## Code review 平台
本專案使用 JetBrains 的 UpSource 服務做為本專案的Code review工具

## Git comment 規範
狀態分為以下幾種：
1.  feat: 新增/修改功能 (feature)。
2.  del: 移除功能或檔案。
3.  fix: 修補 bug (bug fix)。
4.  docs: 文件 (documentation)。
5.  style: 格式 (不影響程式碼運行的變動 white-space, formatting, missing semi colons, etc)。
6.  refactor: 重構 (既不是新增功能，也不是修補 bug 的程式碼變動)。
7.  perf: 改善效能 (A code change that improves performance)。
8.  test: 增加測試 (when adding missing tests)。
9.  chore: 建構程序或輔助工具的變動 (maintain)。
10. revert: 撤銷回覆先前的 commit 例如：revert: type(scope): subject (回覆版本：xxxx)。
11. tmp: 暫存，用於不同電腦間同步未完成的程式碼時使用。

git comment 內容格式如下：

「狀態」（必須，從上述狀態選擇最符合的狀態）：標題（必須，此comment的簡短描述，不要超過50字，結尾不句號）

問題：／新的需求：（二擇一，必須）

原因：（當上方是「問題」時，需要寫這個）

調整項目：（必須）

任務編號：（如有相關任務編號則為必填）

## 命名準則
[常見命名規則教學網頁](https://shunnien.github.io/2017/06/07/naming-conventions/)
1. 資料夾命名原則一律是 `大駝峰`式命名.