# import sys
# import time
# import requests
# import json
# import csv
# from html.parser import HTMLParser
# from datetime import datetime
#
#
# # 過濾HTML標籤，先定義好
# class HTMLCleaner(HTMLParser):
#     def __init__(self, *args, **kwargs):
#         super(HTMLCleaner, self).__init__(*args, **kwargs)
#         self.data_list = []
#
#     def handle_data(self, data):
#         self.data_list.append(data)
#
#
# # 取得網址設定
# def getUrl():
#     # Request URL 請求資源的位置
#     return "https://ppubs.uspto.gov/dirsearch-public/searches/searchWithBeFamily"
#
#
# # 取得HTTP的標頭
# def getHeaders():
#     # User-Agent: 瀏覽器名版本 & 作業系統名版本
#     return {
#         "user-agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36", }
#
#
# # 取得Body設定檔案  start = 爬的起始點，預設是0(可以不給參數) ; end = 結束點預設是7530(可以不給參數)
# def getFormData(start=0, end=7530):
#     return {
#         "start": start,
#         "pageCount": end,
#         "docFamilyFiltering": "familyIdFiltering",
#         "familyIdEnglishOnly": "true",
#         "familyIdFirstPreferred": "US-PGPUB",
#         "familyIdSecondPreferred": "USPAT",
#         "familyIdThirdPreferred": "FPRS",
#         "sort": "date_publ desc",
#         "docFamilyFiltering": "familyIdFiltering",
#         "searchType": 1,
#         "familyIdEnglishOnly": "true",
#         "familyIdFirstPreferred": "US-PGPUB",
#         "familyIdSecondPreferred": "USPAT",
#         "familyIdThirdPreferred": "FPRS",
#         "showDocPerFamilyPref": "showEnglish",
#         "queryId": 0,
#         "tagDocSearch": "false",
#         "query": {
#             "hl_snippets": "2",
#             "op": "OR",
#             "q": "(\"UNMANNED AERIAL VEHICLE\".ti. OR UAV.ti. OR \"UNMANNED AIRCRAFT\".ti. OR DRONE.ti.) AND (\"UNMANNED AERIAL VEHICLE\".ab. OR UAV.ab. OR \"UNMANNED AIRCRAFT\".ab. OR DRONE.ab.)",
#             "queryName": "(\"UNMANNED AERIAL VEHICLE\".ti. OR UAV.ti. OR \"UNMANNED AIRCRAFT\".ti. OR DRONE.ti.) AND (\"UNMANNED AERIAL VEHICLE\".ab. OR UAV.ab. OR \"UNMANNED AIRCRAFT\".ab. OR DRONE.ab.)",
#             "highlights": "1",
#             "qt": "brs",
#             "spellCheck": "false",
#             "viewName": "tile",
#             "plurals": "true",
#             "britishEquivalents": "true",
#             "databaseFilters": [
#                 {
#                     "databaseName": "US-PGPUB",
#                     "countryCodes": []
#                 },
#                 {
#                     "databaseName": "USPAT",
#                     "countryCodes": []
#                 },
#                 {
#                     "databaseName": "USOCR",
#                     "countryCodes": []
#                 }
#             ],
#             "searchType": 1,
#             "ignorePersist": "true",
#             "userEnteredQuery": "(\"UNMANNED AERIAL VEHICLE\".ti. OR UAV.ti. OR \"UNMANNED AIRCRAFT\".ti. OR DRONE.ti.) AND (\"UNMANNED AERIAL VEHICLE\".ab. OR UAV.ab. OR \"UNMANNED AIRCRAFT\".ab. OR DRONE.ab.) \u000b \u000b <br>",
#         }}
#
#
# # 檢測輸入的內容是否正確
# def filterInput(num: str):
#     if num.isdigit():
#         if int(num) < 0:
#             print("您輸入的參數錯誤請輸入 N 或是 大於等於0的數字")
#             sys.exit(-1)
#         elif int(num) > 0:
#             return int(num) + 1
#         else:
#             return int(num)
#     elif num == 'N' or num == 'n':
#         return 0
#     else:
#         print("您輸入的參數錯誤請輸入 N 或是 大於等於0的數字")
#         sys.exit(-1)
#
#
# # 過濾標籤用，會被呼叫
# def filterHtml(data):
#     cleaner = HTMLCleaner()
#     cleaner.feed(data)
#     return [''.join(str(j) for j in cleaner.data_list if j)]
#
#
# # 取得專利的網址
# def getPatentUrl():
#     return "https://ppubs.uspto.gov/dirsearch-public/patents/" + str(
#         backData["patents"][i]["guid"]) + "/highlight?queryId=14700752&source=" + str(
#         backData["patents"][i]["type"]) + "&includeSections=true&uniqueId="
#
#
# # 取得專利的Headers
# def getPatentHeaders():
#     return {
#         "path": "/dirsearch-public/patents/" + str(
#             backData["patents"][i]["guid"]) + "/highlight?queryId=14700752&source=" + str(
#             backData["patents"][i]["type"]) + "&includeSections=true&uniqueId=",
#     }
#
#
# # 主程式
# if __name__ == '__main__':
#     start_num = input("請輸入您上次爬蟲結尾數字(如果是初次使用請輸入0，如果不知道可以輸入 N 預設從0開始): ")
#     print("系統將從 " + str(filterInput(start_num)) + " 開始，若有錯誤將直接終止程式.")
#     print("開始爬取....需等候幾分鐘.")
#     responses = requests.post(getUrl(), headers=getHeaders(), json=getFormData(filterInput(start_num)))
#     backData = json.loads(responses.text)  # 資料存成變數
#     count = filterInput(start_num)
#
#     with open('web.csv', 'a', newline='', encoding='utf-8-sig') as csvfile:
#         # 建立 CSV 檔寫入器
#         writer = csv.writer(csvfile)
#         # 初次使用需先寫入標題
#         title_list = ['申請日', '專利名稱', '摘要']
#         writer.writerow(title_list)
#
#         for i in range(len(backData["patents"])):
#             # print(b["patents"][i]["guid"]+'\n'+b["patents"][i]["type"])
#             d = requests.get(getPatentUrl(), headers=getPatentHeaders())
#             d.content.decode()
#             c = json.loads(d.text)
#
#             print("以下是過濾之後的內容")
#
#             # 過濾HTML標籤
#             # 時間區塊
#             date_list = filterHtml(c['datePublished'])
#             date_list[0] = date_list[0][0:10]
#             print(date_list)
#
#             # 專利名稱區塊
#             name_list = filterHtml(c['inventionTitle'])
#             print(name_list)
#
#             # 專利摘要區塊
#             abstract_list = filterHtml(c['abstractHtml'])
#             print(abstract_list)
#
#             # 合併中
#             date_list.extend(name_list)
#             date_list.extend(abstract_list)
#             print("所有List合併完成，開始寫入!")
#             print("您當前筆數: " + str(count) + " ，請下次開始的時候輸入這個數字系統將會自動從下一筆開始爬!")
#             print("以下為寫入資料")
#             print(date_list)
#             writer.writerow(date_list)
#             print("")
#             count += 1


import sys
import time
import requests
import json
import csv
from html.parser import HTMLParser
from datetime import datetime


# 過濾HTML標籤，先定義好
class HTMLCleaner(HTMLParser):
    def __init__(self, *args, **kwargs):
        super(HTMLCleaner, self).__init__(*args, **kwargs)
        self.data_list = []

    def handle_data(self, data):
        self.data_list.append(data)


# 取得網址設定
def getUrl():
    # Request URL 請求資源的位置
    return "https://ppubs.uspto.gov/dirsearch-public/searches/searchWithBeFamily"


# 取得HTTP的標頭
def getHeaders():
    # User-Agent: 瀏覽器名版本 & 作業系統名版本
    return {
        "user-agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36", }


# 取得Body設定檔案  start = 爬的起始點，預設是0(可以不給參數) ; end = 結束點預設是7530(可以不給參數)
def getFormData(start=0, end=7530):
    return {
        "start": start,
        "pageCount": end,
        "docFamilyFiltering": "familyIdFiltering",
        "familyIdEnglishOnly": "true",
        "familyIdFirstPreferred": "US-PGPUB",
        "familyIdSecondPreferred": "USPAT",
        "familyIdThirdPreferred": "FPRS",
        "sort": "date_publ desc",
        "docFamilyFiltering": "familyIdFiltering",
        "searchType": 1,
        "familyIdEnglishOnly": "true",
        "familyIdFirstPreferred": "US-PGPUB",
        "familyIdSecondPreferred": "USPAT",
        "familyIdThirdPreferred": "FPRS",
        "showDocPerFamilyPref": "showEnglish",
        "queryId": 0,
        "tagDocSearch": "false",
        "query": {
            "hl_snippets": "2",
            "op": "OR",
            # "q"、"queryName"關鍵詞設定(請輸入要檢索之關鍵詞)
            "q": "(UAV)",
            "queryName": "(UAV)",
            "highlights": "1",
            "qt": "brs",
            "spellCheck": "false",
            "viewName": "tile",
            "plurals": "true",
            "britishEquivalents": "true",
            "databaseFilters": [
                {
                    "databaseName": "US-PGPUB",
                    "countryCodes": []
                },
                {
                    "databaseName": "USPAT",
                    "countryCodes": []
                },
                {
                    "databaseName": "USOCR",
                    "countryCodes": []
                }
            ],
            "searchType": 1,
            "ignorePersist": "true",
            # "userEnteredQuery"關鍵詞設定(請輸入要檢索之關鍵詞)
            "userEnteredQuery": "(UAV)",
        }}


# 檢測輸入的內容是否正確
def filterInput(num: str):
    if num.isdigit():
        if int(num) < 0:
            print("您輸入的參數錯誤請輸入 N 或是 大於等於0的數字")
            sys.exit(-1)
        elif int(num) > 0:
            return int(num) + 1
        else:
            return int(num)
    elif num == 'N' or num == 'n':
        return 0
    else:
        print("您輸入的參數錯誤請輸入 N 或是 大於等於0的數字")
        sys.exit(-1)


# 過濾標籤用，會被呼叫
def filterHtml(data):
    cleaner = HTMLCleaner()
    cleaner.feed(data)
    return [''.join(str(j) for j in cleaner.data_list if j)]


# 取得專利的網址
def getPatentUrl():
    return "https://ppubs.uspto.gov/dirsearch-public/patents/" + str(
        backData["patents"][i]["guid"]) + "/highlight?queryId=14700752&source=" + str(
        backData["patents"][i]["type"]) + "&includeSections=true&uniqueId="


# 取得專利的Headers
def getPatentHeaders():
    return {
        "path": "/dirsearch-public/patents/" + str(
            backData["patents"][i]["guid"]) + "/highlight?queryId=14700752&source=" + str(
            backData["patents"][i]["type"]) + "&includeSections=true&uniqueId=",
    }


# 確認傳過來的值是否為空值
def checkValueEmpty(item: str):
    return item if item else '無資料'


# 主程式
if __name__ == '__main__':
    start_num = input("請輸入您上次爬蟲結尾數字(如果是初次使用請輸入0，如果不知道可以輸入 N 預設從0開始): ")
    print("系統將從 " + str(filterInput(start_num)) + " 開始，若有錯誤將直接終止程式.")
    print("開始爬取....需等候幾分鐘.")
    responses = requests.post(getUrl(), headers=getHeaders(), json=getFormData(filterInput(start_num)))
    backData = json.loads(responses.text)  # 資料存成變數
    count = filterInput(start_num)

    with open('web.csv', 'a', newline='', encoding='utf-8-sig') as csvfile:
        # 建立 CSV 檔寫入器
        writer = csv.writer(csvfile)
        # 初次使用需先寫入標題
        title_list = ['申請日', '專利名稱', '摘要']
        writer.writerow(title_list)

        for i in range(len(backData["patents"])):
            # print(b["patents"][i]["guid"]+'\n'+b["patents"][i]["type"])
            d = requests.get(getPatentUrl(), headers=getPatentHeaders())
            d.content.decode()
            c = json.loads(d.text)

            print("以下是過濾之後的內容")

            # 過濾HTML標籤
            # 時間區塊
            date_list = filterHtml(checkValueEmpty(c['datePublished']))
            date_list[0] = date_list[0][0:10]
            print(date_list)

            # 專利名稱區塊
            name_list = filterHtml(checkValueEmpty(c['inventionTitle']))
            print(name_list)

            # 專利摘要區塊
            abstract_list = filterHtml(checkValueEmpty(c['abstractHtml']))
            print(abstract_list)

            # 合併中
            date_list.extend(name_list)
            date_list.extend(abstract_list)
            print("所有List合併完成，開始寫入!")
            print("您當前筆數: " + str(count) + " ，請下次開始的時候輸入這個數字系統將會自動從下一筆開始爬!")
            print("以下為寫入資料")
            print(date_list)
            writer.writerow(date_list)
            print("")
            count += 1
