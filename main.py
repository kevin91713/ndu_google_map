#Main.py   1019開始爬
import random
import time
import json

import requests
from requests import session



# 過濾所有會造成讀取時錯誤的可能
def replaceRes(str):
    str = str.replace('[\'', '')
    str = str.replace('\'', '')
    str = str.replace(']', '')
    str = str.replace(' ', '')
    str = str.replace(',', ' ')
    str = str.replace('，', ' ')
    str = str.replace('=', ' ')
    return str

def getGoogleMapComment(inputPage):
    s = session()
    stopRes = ")]}'\n[null,null,null,"
    counter = inputPage
    pagetext = ""
    pretext = ')]}\''

    strUrl = 'https://www.google.com/maps/preview/review/listentitiesreviews?authuser=0&hl=zh-TW&gl=tw&pb=!1m2!1y3776411792604063727!2y15986087103770438193!2m2!2i10!1i' + str(counter) + '0!3e1!4m5!3b1!4b1!5b1!6b1!7b1!5m2!1snz9VY5KVLNK6hwPI27tI!7e81'


    num = 0
    # url = "https://www.google.com.tw/maps/preview/review/listentitiesreviews?authuser=0&hl=zh-TW&gl=tw&pb=!1m2!1y3765763519716247371!2y6632645166790586489!2m2!1i0!2i10!3e1!4m5!3b1!4b1!5b1!6b1!7b1!5m2!1sRn2TYpaIFam02roP06aPGA!7e81"
    # url = "https://www.google.com.tw/maps/preview/review/listentitiesreviews?authuser=0&hl=zh-TW&gl=tw&pb=!1m2!1y3765763519716247371!2y6632645166790586489!2m2!1i" + str(
    #     counter) + "!2i10!3e1!4m5!3b1!4b1!5b1!6b1!7b1!5m2!1sRn2TYpaIFam02roP06aPGA!7e81"
    # url = 'https://www.google.com.tw/maps/preview/review/listentitiesreviews?authuser=0&hl=zh-TW&gl=tw&authuser=0&pb=!1m2!1y3777493615137947639!2y10991755371936558003!2m2!2i10!1i' + str(counter) + '0!3e1!4m5!3b1!4b1!5b1!6b1!7b1!5m2!1sEAowY8muDOyzmAXFobWgBg!7e81'
    url = strUrl
    # requests.get(url,proxies=)
    r = s.get(url.format(pagetext)).text
    while r.startswith(stopRes) is not True:
        # uurl = "https://www.google.com.tw/maps/preview/review/listentitiesreviews?authuser=0&hl=zh-TW&gl=tw&pb=!1m2!1y3765763519716247371!2y6632645166790586489!2m2!1i" + str(
        # counter) + "!2i10!3e1!4m5!3b1!4b1!5b1!6b1!7b1!5m2!1sRn2TYpaIFam02roP06aPGA!7e81"
        uurl = 'https://www.google.com/maps/preview/review/listentitiesreviews?authuser=0&hl=zh-TW&gl=tw&pb=!1m2!1y3776411792604063727!2y15986087103770438193!2m2!2i10!1i' + str(counter) + '0!3e1!4m5!3b1!4b1!5b1!6b1!7b1!5m2!1snz9VY5KVLNK6hwPI27tI!7e81'

        if num > 0:
            time.sleep(random.uniform(300, 360))
        else:
            #加速第一次取得資料，否則等太久然後又顯示錯誤
            time.sleep(random.uniform(5, 10))
            num += 1
        # time.sleep(random.uniform(300, 360))
        # time.sleep(random.uniform(5, 10))
        counter = counter + 1
        pagetext = str(counter)

        text = s.get(uurl).text
        text = text.replace(pretext, '')
        # print(text)
        soup = json.loads(text)
        conlist = soup[2]
        # 檔案寫入
        # print(conlist)
        file = open("GoogleMapComment.txt", "a",encoding='UTF-8')

        array = []
        writeArray = []
        j = 0
        for i in conlist:
            print("username:" + str(i[0][1]))
            print("time:" + str(i[1]))
            print("comment:" + str(i[3]))
            print("score:" + str(i[4]))
            print("\r\n")
            print(counter)
            print("\r\n")
            # 以下是處理寫入檔案
            array.append(array.append(replaceRes(i[0][1])))
            array.append(i[1])
            array.append(replaceRes(str(i[3])))
            array.append(i[4])
            newarray = array.copy()
            writeArray.append(newarray)
            array.clear()
            print("開始寫入")
            print(writeArray)
            print("以下為新的一筆資料")
            print("\r\n")
            file.write(''.join(str(writeArray[j])))
            file.write("=")
            j += 1
        file.close()



inputPage = eval(input("請輸入您上次爬蟲結尾數字(如果是初次使用請輸入0): "))
print("您所輸入的數字是: " + str(inputPage) + ", 系統將開始從上次結尾開始爬 !")
file = open("GoogleMapComment.txt", "a")
if inputPage >= 0:
    getGoogleMapComment(inputPage)
else:
    print("數字輸入錯誤")

