import csv
import re
import demoji


# ReadFile.py
def replaceRes(text):
    text = text.replace('[\'', '')
    text = text.replace('\'', '')
    text = text.replace(']', '')
    text = text.replace(' ', '')
    text = text.replace('\\n', ' ')
    text = text.replace('＞', ' ')
    text = text.replace('＜', ' ')
    text = text.replace('（', ' ')
    text = text.replace('）', ' ')

    # 除錯用(不可以刪除)
    # print("偵測到的表情符號: ")
    # print(demoji.findall(text))
    # print("清除後: ")
    # print(demoji.replace(text))

    return demoji.replace(text)
    # 過濾表情符號(舊版會有遺漏)
    # regrex_pattern = re.compile(pattern="["
    #                                     u"\U0001F600-\U0001F64F"  # emoticons
    #                                     u"\U0001F300-\U0001F5FF"  # symbols & pictographs
    #                                     u"\U0001F680-\U0001F6FF"  # transport & map symbols
    #                                     u"\U0001F1E0-\U0001F1FF"  # flags (iOS)
    #                                     u"\U00002702-\U000027B0"
    #                                     u"\U0001f926-\U0001f937"
    #                                     u'\U00010000-\U0010ffff'
    #                                     u"\u200d"
    #                                     u"\u2640-\u2642"
    #                                     u"\u2600-\u2B55"
    #                                     u"\u23cf"
    #                                     u"\u23e9"
    #                                     u"\u231a"
    #                                     u"\u3030"
    #                                     u"\ufe0f"
    #                                     "]+", flags=re.UNICODE)
    # return regrex_pattern.sub(r'', text)
    # print(text)


array = open("GoogleMapComment.txt", "r", encoding='ISO-8859-1').read()
array = array.split('=')

# 開啟輸出的 CSV 檔案
with open('GoogleMapComment.csv', 'w', newline='', encoding='utf-8-sig') as csvfile:
    # 建立 CSV 檔寫入器
    writer = csv.writer(csvfile)
    # 必須先弄好一個陣列先隨便塞元素進去反正等等會取代掉( * 4 => 快速將陣列四格都填寫0 )
    arrayData = [0] * 4
    j = 0
    # 可以把資料先塞到陣列，塞到CSV之後就繼續塞下一筆資料
    for i in array:
        if i != '':
            print("原始資料: ")
            print(i)
            print("\r\n")
            for data in i.split(','):
                # 跟之前的一樣只是這邊是給到陣列
                # print(arrayData[j])
                # print("\r\n")
                # print(replaceRes(data))
                # print("\r\n")
                arrayData[j] = replaceRes(data)
                # 這邊必須加一沒有加一的話你的值會被下一個值取代。
                j += 1
            print("過濾後的資料")
            print(arrayData)
            # 將剛剛的陣列寫入到CSV
            writer.writerow(arrayData)
            # 歸零否則會超出索引值
            j = 0
            print("\r\n")
