# import random
# import sys
# import time
# import json
# import requests
# from requests import session
# from fake_useragent import UserAgent
#
#
# # 過濾資料避免寫入時候出現錯誤
# def replaceData(data):
#     return data.replace('[\'', '').replace('\'', '').replace(']', '') \
#         .replace(' ', '').replace(',', ' ').replace('，', ' ').replace('=', ' ')
#
#
# # 更換 proxy ip
# def changeProxyIP():
#     return {0: ipD[int(random.uniform(0, len(ipArray) - 1))]}
#
#
# # 取得網址
# def getGoogleLandMarkUrl(num):
#     # 使用者需要預先定義好的地標網址
#     return 'https://www.google.com.tw/maps/preview/review/listentitiesreviews?authuser=0&hl=zh-TW&gl=tw&pb=!1m2!1y3765763519716247371!2y6632645166790586489!2m2!2i10!1i' + str(
#         num) + '0!3e1!4m5!3b1!4b1!5b1!6b1!7b1!5m2!1sU2aEY-RiyaCHA_PHppAI!7e81'
#
#
# # 爬蟲階段
# def getGoogleMapComment(userInput):
#     # 爬蟲一旦判定到這串就會停止
#     stopRes = ")]}'\n[null,null,null,"
#     counter = int(userInput)
#     pagetext = ""
#     pretext = ')]}\''
#
#     # 隨機取得一組proxy ip
#     proxyIP = changeProxyIP()
#     proxies = {
#         "http": "http://" + proxyIP.get(0),
#         "https": "https://" + proxyIP.get(0),
#     }
#
#     # Request 偽造成瀏覽器格式
#     ua = UserAgent()
#     header = {"User-Agent": ua.random}
#
#     # response = requests.get(getGoogleLandMarkUrl(counter), proxies=proxies, headers=header).text
#     response = requests.get(getGoogleLandMarkUrl(counter), headers=header).text
#     while response.startswith(stopRes) is not True:
#     # while counter <= 2000:
#         # 每爬 100 筆資料都去做一次更新IP 跟 更新送出請求表頭更新
#         if int(counter) - int(userInput) >= 100:
#             proxyIP = changeProxyIP()
#             header = {"User-Agent": ua.random}
#             userInput = counter
#             print("您的IP已經幫您更換至: " + str(proxyIP.get(0)))
#             print("您的Request已經更新: " + str(header))
#         counter += 1
#
#         # text = requests.get(getGoogleLandMarkUrl(counter), proxies=proxyIP, headers=header).text
#         text = requests.get(getGoogleLandMarkUrl(counter), headers=header).text
#         text = text.replace(pretext, '')
#         try:
#             soup = json.loads(text)
#         except:
#             print("爬蟲失敗")
#             print("提醒您:系統將自動重新啟動爬蟲!如果多次爬蟲失敗請更換您當前IP再次嘗試!")
#             getGoogleMapComment(counter - 1)
#             break
#         conList = soup[2]
#
#         array = []
#         writeArray = []
#         j = 0
#         for i in conList:
#             print(
#                 f"username: {str(i[0][1])} \r\n time: {str(i[1])} \r\n comment: {str(i[3])} \r\n score: {str(i[4])} \r\n {counter} \r\n")
#             # 以下是處理寫入檔案
#             array.append(replaceData(i[0][1]))
#             array.append(i[1])
#             array.append(replaceData(str(i[3])))
#             array.append(i[4])
#             newarray = array.copy()
#             writeArray.append(newarray)
#             array.clear()
#             print("開始寫入")
#             print(writeArray)
#             print("以下為新的一筆資料")
#             print("\r\n")
#             file.write(''.join(str(writeArray[j])))
#             file.write("=")
#             j += 1
#         # 設定延遲
#         # time.sleep(random.uniform(300, 360))
#         time.sleep(random.uniform(30, 32))
#     file.close()
#
#
# # 程式開始執行
# inputPage = input("請輸入您上次爬蟲結尾數字(如果是初次使用請輸入0): ")
# if inputPage.isdigit():
#     # 必須先定義好的
#
#     # ipTxt的資料先載入到記憶體裡面去等等操作會比較快
#     ipArray = []
#     for i in open("proxyTXT.txt", "r", encoding='UTF-8'):
#         ipArray.append(i.replace('\n', ''))
#     # 給requests用的
#     ipD = {i: ipArray[i] for i in range(len(ipArray))}
#
#     # 使用者輸入的數字，預設為: 0
#     counter = 0
#
#     print("您所輸入的數字是: " + str(inputPage) + ", 系統將開始從上次結尾開始爬 !")
#     file = open("GoogleMapComment.txt", "a", encoding='UTF-8')
#     getGoogleMapComment(inputPage)
# else:
#     print("[提示] : 您必須輸入的是10進位的數字!")
