import pandas as pd
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.metrics.pairwise import cosine_similarity
from sklearn.feature_extraction.text import TfidfVectorizer
# Excel 處理
from openpyxl import load_workbook

excel_file = load_workbook(filename='01.xlsx')
sheet = excel_file.worksheets[0]
sheet1 = excel_file.worksheets[1]
# print('工作表名稱: ', sheet)
# print('row總數:', sheet.max_row)
# print('column總數:', sheet.max_column)
# work_sheet_1 = [sheet.cell(i, 3).value for i in range(1, sheet.max_row + 1)]
a = {'A' + str(i): sheet.cell(i, 3).value for i in range(1, sheet.max_row + 1)}
b = {'B' + str(i): sheet1.cell(i, 3).value for i in range(1, sheet1.max_row + 1)}

data = [j for i, j in a.items()]
for i, j in b.items():
    data.append(j)

id_name = [i for i, j in a.items()]
for i, j in b.items():
    id_name.append(i)

writer = pd.ExcelWriter("02.xlsx")

# for i, j in a.items():
#     print(i)
#     print(j)

# doc_1 = "system, method, biological, extract, control, capture, aircraft, flight, recovery, supplement, medicine, pharmaceutical, industry, dietary, food, bad, biologically, valuethe, active, launch"
# doc_2 = "soil, solvent, explosive, filter, system, control, method, flight, uavs, site, device, spill, electrochemical, ethylene, spectroscopy, voltammetry, suspect, eutectic, choline, inexpensive"
#
# data = [doc_1, doc_2]

count_vectorizer = CountVectorizer()
vector_matrix = count_vectorizer.fit_transform(data)

tokens = count_vectorizer.get_feature_names_out()

vector_matrix.toarray()


def create_dataframe(matrix, tokens):
    doc_names = [f'doc_{i + 1}' for i, _ in enumerate(matrix)]
    df = pd.DataFrame(data=matrix, index=id_name, columns=tokens)
    return (df)


# create_dataframe(vector_matrix.toarray(), tokens)

cosine_similarity_matrix = cosine_similarity(vector_matrix)
create_dataframe(cosine_similarity_matrix, id_name).to_excel(writer, sheet_name='Sheet', index=True)
writer.close()

# Tfidf_vect = TfidfVectorizer()
# vector_matrix = Tfidf_vect.fit_transform(data)

# tokens = Tfidf_vect.get_feature_names_out()
# create_dataframe(vector_matrix.toarray(), tokens)
# cosine_similarity_matrix = cosine_similarity(vector_matrix)
# writer = pd.ExcelWriter("02.xlsx")
# create_dataframe(cosine_similarity_matrix, id_name).to_excel(writer, sheet_name='Sheet', index=True)
# writer.close()
